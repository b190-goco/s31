
/*
	"require()" directive
		- loads Node.js modules
	"http"
		- lets Node.js transfer data using Hyper Text Transfer Protocol
		- set of individual files that contain code to create a "component" that helps establish data transfer between applications
	HTTP
		- protocol that allows fetching of resources such as HTML documents

	clients (browser) & server (Node.js/Express.js applications) communicate by exhanging individual messages

	message from client
		- called "request"

	message from server
		- called "response"

*/
let http = require("http");
/*
	http - trying to use http module to create server-side application

	createServer() - found inside http module; a method that accpets a function as its argument for a creation of a server

	(request, response) - arguments that are passed to the createServer method; allows to receive request and send response
*/
http.createServer(function (request, response){
// .listen() - allows our application to be run in our local devices through a specified port
// port -  virtual point where network connections start and end; - each port is associated with a specific process/service
// the code below means
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Hello World");

}).listen(4000);
/*
	node index.js
		- use to run server
	press ctrl + c to terminate the gitbash process
*/
// used to confirm if the server is running on a port
console.log("Server running at port: 4000");
